module.exports = {
  future: {
    // removeDeprecatedGapUtilities: true,
    // purgeLayersByDefault: true,
  },
  purge: [],
  theme: {
    extend: {
      colors: {
        'black-olive': '#37392e',
        'blue-sapphire': '#19647e',
        'veridigris-green': '#28aFb0',
        'pale-warm-silver': '#ddcecd',
        'isabelline-ivory': '#eee5e5',

        'fire-opal': '#EF6461',
        'sunray': '#E4B363',
        'platinum': '#E8E9EB',
        'alabaster': '#E0DFD5',
        'onyx': '#313638',

        'light-pink': '#FFB1B6',
        'salmon-pink': '#FF818F',
        'black-coffee': '#413536',
        'umber': '#644E46',
        'beau-blue': '#C9E4FA',
        'gold-crayola': '#FFCB7D',

        'btn-hover': '#FFB1B6',
      },

      spacing: {
        'card-x': '360px',
        'card-y': '570.54px',
        'card': '158.48vw',
        'card-b': '24%',
        'profile': '83%',
      },

      fontFamily: {
        'tajawal': ['Tajawal'],
        'nerko-one': ['Nerko One'],
      },

      keyframes: {
        fadeIn: {
          '0%': { opacity: '0' , transform: 'rotateX(20deg)' },
          '100%': { opacity: '1' , transform: 'rotateX(0)' },
        },
        wiggle: {
           '0%, 100%': { transform: 'rotate(-3deg)' },
           '50%': { transform: 'rotate(3deg)' },
         }
      },

      animation: {
        firstFadeIn: 'fadeIn 1s ease-in-out',
        secondFadeIn: 'fadeIn 1.3s ease-in-out',
        wiggle: 'wiggle 1s ease-in-out infinite',
      },
    },
  },
  variants: {},
  plugins: [],
}
